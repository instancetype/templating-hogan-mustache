/**
 * Created by instancetype on 6/20/14.
 */
var hogan = require('hogan.js')
  , md = require('github-flavored-markdown')

var template = '{{#markdown}}'
             + '**Name**: {{name}}'
             + '{{/markdown}}'

var context = { name: 'The Jitsu Monster'
              , markdown: function() {
                  return function(text) {
                    return md.parse(text)
                  }
              }}

template = hogan.compile(template)
console.log(template.render(context))