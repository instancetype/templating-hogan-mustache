/**
 * Created by instancetype on 6/20/14.
 */
var hogan = require('hogan.js')
  , template = '{{message}}'
  , context = {message: 'Hello, template!'}

template = hogan.compile(template)
console.log(template.render(context))