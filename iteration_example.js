/**
 * Created by instancetype on 6/20/14.
 */
var hogan = require('hogan.js')
  , template = '{{#students}}<p>Name: {{name}} Age: {{age}} years old</p>{{/students}}'
             + '{{^students}}<p>No students found</p>{{/students}}'
  , context = { students: [ { name: 'Jitsu', age: 5 }
                          , { name: 'Kalymba', age: 11 }
                          ]}

template = hogan.compile(template)
console.log(template.render(context))
setTimeout(function() {
  console.log('removing students to display inverted section...')

  setTimeout(function() {
    context = {}
    console.log(template.render(context))
  }, 1000)

}, 1000)


