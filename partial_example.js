/**
 * Created by instancetype on 6/20/14.
 */
var hogan = require('hogan.js')

var catsTemplate = '<p>Name: {{name}}, Age: {{age}}</p>'

var mainTemplate = '{{#cats}}{{>cat}}{{/cats}}'

var context = { cats: [ { name: 'Jitsu', age: 5 }
                      , { name: 'Kalymba', age: 11 }
                      ]}

var template = hogan.compile(mainTemplate)
  , partial = hogan.compile(catsTemplate)

var html = template.render(context, {cat: partial})
console.log(html)